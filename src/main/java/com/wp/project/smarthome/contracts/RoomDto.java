package com.wp.project.smarthome.contracts;

public class RoomDto {

    private String uid;

    private String name;

    private Integer floor;

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return this.uid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Integer getFloor() {
        return this.floor;
    }
}
