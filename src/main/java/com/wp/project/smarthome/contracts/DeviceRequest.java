package com.wp.project.smarthome.contracts;

public class DeviceRequest {

    private String referenceId;

    private String name;

    private String location;

    private Long deviceType;

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getReferenceId() {
        return this.referenceId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return this.location;
    }

    public void setDeviceType(Long deviceType) {
        this.deviceType = deviceType;
    }

    public Long getDeviceType() {
        return this.deviceType;
    }
}
