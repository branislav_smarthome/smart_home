package com.wp.project.smarthome.models;

import com.wp.project.smarthome.contracts.DeviceRequest;
import com.wp.project.smarthome.repository.exceptions.DeviceNotFoundException;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String uid;

    private String name;

    private Integer floor;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "room_id")
    private List<Device> devices = new ArrayList<>();

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return this.uid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Integer getFloor() {
        return this.floor;
    }

    public Room addDevice(DeviceRequest request, DeviceType deviceType) {
        Device device = new Device();
        device.setUid(UUID.randomUUID().toString());
        device.setName(request.getName());
        device.setLocation(request.getLocation());
        device.setReferenceId(request.getReferenceId());
        device.setDeviceType(deviceType);
        this.devices.add(device);
        return this;
    }

    public Room updateDevice(String deviceUid, DeviceRequest request, DeviceType deviceType) {
        Device dbDevice = getDevice(deviceUid);

        dbDevice.setReferenceId(request.getReferenceId());
        dbDevice.setLocation(request.getLocation());
        dbDevice.setName(request.getName());
        dbDevice.setDeviceType(deviceType);
        return this;
    }

    public Room removeDevice(String deviceUid) {
        Device dbDevice = getDevice(deviceUid);
        this.devices.remove(dbDevice);
        return this;
    }

    public Device getDevice(String deviceUid) {
        return this.devices.stream().filter(x -> x.getUid().equals(deviceUid)).findFirst().orElseThrow(DeviceNotFoundException::new);
    }

    public List<Device> getDevices() {
        return this.devices;
    }
}