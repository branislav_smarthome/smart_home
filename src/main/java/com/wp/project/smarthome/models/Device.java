package com.wp.project.smarthome.models;

import com.wp.project.smarthome.contracts.DeviceAttributeRequest;
import com.wp.project.smarthome.repository.exceptions.DeviceAttributeNotFoundException;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String uid;

    private String referenceId;

    private String name;

    private String location;

    @OneToOne
    @JoinColumn(name = "devicetype_id", referencedColumnName = "id")
    private DeviceType deviceType;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "device_id")
    private List<DeviceAttribute> deviceAttributes = new ArrayList<>();

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return this.uid;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getReferenceId() {
        return this.referenceId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return this.location;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public DeviceType getDeviceType() {
        return this.deviceType;
    }

    public Device addAttribute(DeviceAttributeRequest request) {
        DeviceAttribute deviceAttribute = new DeviceAttribute();
        deviceAttribute.setUid(UUID.randomUUID().toString());
        deviceAttribute.setKey(request.getKey());
        deviceAttribute.setValue(request.getValue());
        this.deviceAttributes.add(deviceAttribute);
        return this;
    }

    public Device updateAttribute(String deviceAttributeUid, DeviceAttributeRequest request) throws DeviceAttributeNotFoundException {
        DeviceAttribute dbDeviceAttribute = getDeviceAttribute(deviceAttributeUid);
        dbDeviceAttribute.setKey(request.getKey());
        dbDeviceAttribute.setValue(request.getValue());
        return this;
    }

    public Device deleteAttribute(String deviceAttributeUid) throws DeviceAttributeNotFoundException {
        DeviceAttribute dbDeviceAttribute = getDeviceAttribute(deviceAttributeUid);
        this.deviceAttributes.remove(dbDeviceAttribute);
        return this;
    }

    public DeviceAttribute getDeviceAttribute(String deviceAttributeUid) {
        return this.deviceAttributes.stream().filter(x -> x.getUid().equals(deviceAttributeUid)).findFirst().orElseThrow(DeviceAttributeNotFoundException::new);
    }

    public List<DeviceAttribute> getDeviceAttributes() {
        return this.deviceAttributes;
    }
}
