package com.wp.project.smarthome.models;

import com.wp.project.smarthome.contracts.DeviceAttributeRequest;
import com.wp.project.smarthome.contracts.DeviceRequest;
import com.wp.project.smarthome.contracts.RoomRequest;
import com.wp.project.smarthome.repository.exceptions.RoomNotFoundException;
import org.springframework.data.crossstore.ChangeSetPersister;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Entity
public class Workspace {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String uid;

    private  String name;

    private String description;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "workspace_id")
    private List<Room> rooms = new ArrayList<>();

    @ManyToMany(mappedBy = "workspaces")
    private List<User> users = new ArrayList<>();

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return this.uid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public Workspace addRoom(RoomRequest request) {
        Room room = new Room();
        room.setUid(UUID.randomUUID().toString());
        room.setName(request.name);
        room.setFloor(request.floor);
        rooms.add(room);
        return this;
    }

    public Workspace updateRoom(String roomUid, RoomRequest request) {
        Room dbRoom = getRoom(roomUid);
        dbRoom.setFloor(request.floor);
        dbRoom.setName(request.name);
        return this;
    }

    public Workspace deleteRoom(String roomUid) {
        Room room = this.rooms.stream().filter(x -> x.getUid().equals(roomUid)).findFirst().orElseThrow(RoomNotFoundException::new);
        this.rooms.remove(room);
        return this;
    }

    public Room getRoom(String roomUid) {
        return this.rooms.stream().filter(x -> x.getUid().equals(roomUid)).findFirst().orElseThrow(RoomNotFoundException::new);
    }

    public List<Room> getRooms() {
        return this.rooms;
    }

    public Workspace addDeviceInRoom(String roomUid, DeviceRequest request, DeviceType deviceType) {
        Room dbRoom = getRoom(roomUid);
        dbRoom.addDevice(request, deviceType);
        return this;
    }

    public Workspace updateDeviceInRoom(String roomUid, String deviceUid, DeviceRequest request, DeviceType deviceType) {
        Room dbRoom = getRoom(roomUid);
        dbRoom.updateDevice(deviceUid, request, deviceType);
        return this;
    }

    public Workspace deleteDeviceInRoom(String roomUid, String deviceUid) {
        Room dbRoom = getRoom(roomUid);
        dbRoom.removeDevice(deviceUid);
        return this;
    }

    public Device getDeviceInRoom(String roomUid, String deviceUid) {
        return getRoom(roomUid).getDevice(deviceUid);
    }

    public List<Device> getDevicesInRoom(String roomUid) {
        return getRoom(roomUid).getDevices();
    }

    public Workspace addDeviceAttribute(String roomUid, String deviceUid, DeviceAttributeRequest request) {
        Device dbDevice = getDeviceInRoom(roomUid, deviceUid);
        dbDevice.addAttribute(request);
        return this;
    }

    public Workspace updateDeviceAttribute(String roomUid, String deviceUid, String deviceAttributeUid, DeviceAttributeRequest request) {
        Device dbDevice = getDeviceInRoom(roomUid, deviceUid);
        dbDevice.updateAttribute(deviceAttributeUid, request);
        return this;
    }

    public Workspace removeDeviceAttribute(String roomUid, String deviceUid, String deviceAttributeUid) {
        Device dbDevice = getDeviceInRoom(roomUid, deviceUid);
        dbDevice.deleteAttribute(deviceAttributeUid);
        return this;
    }

    public DeviceAttribute getDeviceAttribute(String roomUid, String deviceUid, String deviceAttributeUid) {
        return getDeviceInRoom(roomUid, deviceUid).getDeviceAttribute(deviceAttributeUid);
    }

    public List<DeviceAttribute> getDeviceAttributes(String roomUid, String deviceUid) {
        return getRoom(roomUid).getDevice(deviceUid).getDeviceAttributes();
    }
}
