package com.wp.project.smarthome.repository;

import com.wp.project.smarthome.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

}
