package com.wp.project.smarthome.repository;

import com.wp.project.smarthome.models.DeviceType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceTypeRepository extends JpaRepository<DeviceType, Long> {

}
