package com.wp.project.smarthome.repository;

import com.wp.project.smarthome.models.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WorkspaceRepository extends JpaRepository<Workspace, Long> {
    Optional<Workspace> findByUid(String uid);
}
