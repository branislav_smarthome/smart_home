package com.wp.project.smarthome.web;

import com.wp.project.smarthome.contracts.*;
import com.wp.project.smarthome.models.DeviceAttribute;
import com.wp.project.smarthome.repository.exceptions.DeviceAttributeNotFoundException;
import com.wp.project.smarthome.repository.exceptions.DeviceNotFoundException;
import com.wp.project.smarthome.repository.exceptions.RoomNotFoundException;
import com.wp.project.smarthome.service.DeviceTypeService;
import com.wp.project.smarthome.service.WorkspaceService;

import javassist.NotFoundException;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("api/workspace")
public class WorkspaceController {

    private WorkspaceService workspaceService;
    private DeviceTypeService deviceTypeService;

    public WorkspaceController(WorkspaceService workspaceService,
                               DeviceTypeService deviceTypeService) {
        this.workspaceService = workspaceService;
        this.deviceTypeService = deviceTypeService;
    }

    @PostMapping(value = "", consumes = "application/json")
    public void createWorkspace(@RequestBody WorkspaceRequest request) throws NotFoundException {
        workspaceService.createWorkspace(request);
    }

    @PatchMapping(value = "{workspaceUid}", consumes = "application/json", produces = "application/json")
    public WorkspaceDto updateWorkspace(@PathVariable String workspaceUid, @RequestBody WorkspaceRequest request) throws NotFoundException {
        return workspaceService.updateWorkspace(workspaceUid, request);
    }

    @DeleteMapping(value = "{workspaceUid}")
    public void deleteWorkspace(@PathVariable String workspaceUid) throws NotFoundException {
        workspaceService.deleteWorkspace(workspaceUid);
    }

    @GetMapping(value = "", produces = "application/json")
    public List<WorkspaceDto> getWorkspaces(){
        return workspaceService.getWorkspaces();
    }

    @GetMapping(value="{workspaceUid}", produces = "application/json")
    public WorkspaceDto getWorkspace(@PathVariable String workspaceUid) throws NotFoundException {
        return workspaceService.getWorkspace(workspaceUid);
    }

    @PostMapping(value = "{workspaceUid}/room")
    public void createRoomInWorkspace(@PathVariable String workspaceUid, @RequestBody RoomRequest request) throws NotFoundException {
        workspaceService.addRoomInWorkspace(workspaceUid, request);
    }

    @PatchMapping(value = "{workspaceUid}/room/{roomUid}", consumes = "application/json", produces = "application/json")
    public RoomDto updateRoomInWorkspace(@PathVariable String workspaceUid, @PathVariable String roomUid, @RequestBody RoomRequest request) throws NotFoundException {
        return workspaceService.updateRoomInWorkspace(workspaceUid, roomUid, request);
    }

    @DeleteMapping(value = "{workspaceUid}/room/{roomUid}")
    public void deleteRoomInWorkspace(@PathVariable String workspaceUid, @PathVariable String roomUid) throws NotFoundException, RoomNotFoundException {
        workspaceService.deleteRoomInWorkspace(workspaceUid, roomUid);
    }

    @GetMapping(value = "{workspaceUid}/rooms", produces = "application/json")
    public List<RoomDto> getRoomsInWorkspace(@PathVariable String workspaceUid) throws NotFoundException {
        return workspaceService.getRoomsInWorkspace(workspaceUid);
    }

    @GetMapping(value = "{workspaceUid}/room/{roomUid}")
    public RoomDto getRoomInWorkspace(@PathVariable String workspaceUid, @PathVariable String roomUid) throws NotFoundException {
        return workspaceService.getRoomInWorkspace(workspaceUid, roomUid);
    }

    @PostMapping(value = "{workspaceUid}/room/{roomUid}/device")
    public void createDevice(@PathVariable String workspaceUid, @PathVariable String roomUid, @RequestBody DeviceRequest request) throws NotFoundException {
        workspaceService.addDeviceInRoom(workspaceUid, roomUid, request);
    }

    @PatchMapping(value = "{workspaceUid}/room/{roomUid}/device/{deviceUid}")
    public DeviceDto updateDevice(@PathVariable String workspaceUid, @PathVariable String roomUid, @PathVariable String deviceUid, @RequestBody DeviceRequest request) throws NotFoundException, DeviceNotFoundException {
        return workspaceService.updateDeviceInRoom(workspaceUid, roomUid, deviceUid, request);
    }

    @DeleteMapping(value = "{workspaceUid}/room/{roomUid}/device/{deviceUid}")
    public void deleteDevice(@PathVariable String workspaceUid, @PathVariable String roomUid, @PathVariable String deviceUid) throws NotFoundException, DeviceNotFoundException {
        workspaceService.deleteDeviceInRoom(workspaceUid, roomUid, deviceUid);
    }

    @GetMapping(value = "{workspaceUid}/room/{roomUid}/devices")
    public List<DeviceDto> getDevicesInRoom(@PathVariable String workspaceUid, @PathVariable String roomUid) throws NotFoundException {
        return workspaceService.getDevicesInRoom(workspaceUid, roomUid);
    }

    @GetMapping(value = "{workspaceUid}/room/{roomUid}/device/{deviceUid}")
    public DeviceDto getDeviceInRoom(@PathVariable String workspaceUid, @PathVariable String roomUid, @PathVariable String deviceUid) throws NotFoundException {
        return workspaceService.getDeviceInRoom(workspaceUid, roomUid, deviceUid);
    }

    @PostMapping(value = "{workspaceUid}/room/{roomUid}/device/{deviceUid}/attribute")
    public void createDeviceAttribute(@PathVariable String workspaceUid, @PathVariable String roomUid, @PathVariable String deviceUid, @RequestBody DeviceAttributeRequest request) throws NotFoundException {
        workspaceService.addDeviceAttribute(workspaceUid, roomUid, deviceUid, request);
    }

    @PatchMapping(value = "{workspaceUid}/room/{roomUid}/device/{deviceUid}/attribute/{deviceAttributeUid}")
    public void updateDeviceAttribute(@PathVariable String workspaceUid, @PathVariable String roomUid, @PathVariable String deviceUid, @PathVariable String deviceAttributeUid, @RequestBody DeviceAttributeRequest request) throws NotFoundException {
        workspaceService.updateDeviceAttribute(workspaceUid, roomUid, deviceUid, deviceAttributeUid, request);
    }

    @DeleteMapping(value = "{workspaceUid}/room/{roomUid}/device/{deviceUid}/attribute/{deviceAttributeUid}")
    public void deleteDeviceAttribute(@PathVariable String workspaceUid, @PathVariable String roomUid, @PathVariable String deviceUid, @PathVariable String deviceAttributeUid) throws NotFoundException, DeviceAttributeNotFoundException {
        workspaceService.deleteDeviceAttribute(workspaceUid, roomUid, deviceUid, deviceAttributeUid);
    }

    @GetMapping(value = "{workspaceUid}/room/{roomUid}/device/{deviceUid}/attributes", produces = "application/json")
    public List<DeviceAttributeDto> getDeviceAttributes(@PathVariable String workspaceUid, @PathVariable String roomUid, @PathVariable String deviceUid) throws NotFoundException {
        return workspaceService.getDeviceAttributes(workspaceUid, roomUid, deviceUid);
    }

    @GetMapping(value = "{workspaceUid}/room/{roomUid}/device/{deviceUid}/attribute/{deviceAttributeUid}", produces = "application/json")
    public DeviceAttributeDto getDeviceAttribute(@PathVariable String workspaceUid, @PathVariable String roomUid, @PathVariable String deviceUid, @PathVariable String deviceAttributeUid) throws NotFoundException {
        return workspaceService.getDeviceAttribute(workspaceUid, roomUid, deviceUid, deviceAttributeUid);
    }

    @GetMapping(value = "device-types", produces = "application/json")
    public List<DeviceTypeDto> getDeviceTypes() {
        return deviceTypeService.getDeviceTypes();
    }
}