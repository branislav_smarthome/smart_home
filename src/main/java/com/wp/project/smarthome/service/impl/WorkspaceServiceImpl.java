package com.wp.project.smarthome.service.impl;

import com.wp.project.smarthome.contracts.*;
import com.wp.project.smarthome.models.*;
import com.wp.project.smarthome.repository.DeviceTypeRepository;
import com.wp.project.smarthome.repository.WorkspaceRepository;
import com.wp.project.smarthome.repository.exceptions.DeviceAttributeNotFoundException;
import com.wp.project.smarthome.repository.exceptions.DeviceNotFoundException;
import com.wp.project.smarthome.repository.exceptions.RoomNotFoundException;
import com.wp.project.smarthome.service.WorkspaceService;

import javassist.NotFoundException;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class WorkspaceServiceImpl implements WorkspaceService {

    private WorkspaceRepository workspaceRepository;
    private DeviceTypeRepository deviceTypeRepository;

    public WorkspaceServiceImpl(WorkspaceRepository workspaceRepository,
                                DeviceTypeRepository deviceTypeRepository) {
        this.workspaceRepository = workspaceRepository;
        this.deviceTypeRepository = deviceTypeRepository;
    }

    public void createWorkspace(WorkspaceRequest request) throws NotFoundException {

        if (request == null) {
            throw new NotFoundException("Workspace request is required and cannot be null");
        }

        Workspace workspace = new Workspace();
        workspace.setUid(UUID.randomUUID().toString());
        workspace.setName(request.name);
        workspace.setDescription(request.description);

        workspaceRepository.save(workspace);
    }

    public WorkspaceDto updateWorkspace(String workspaceUid, WorkspaceRequest request) throws NotFoundException {

        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();

        dbWorkplace.setName(request.name);
        dbWorkplace.setDescription(request.description);

        workspaceRepository.save(dbWorkplace);

        return CreateWorkspaceDto(dbWorkplace);
    }

    public void deleteWorkspace(String workspaceUid) throws NotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        workspaceRepository.delete(optionalWorkspace.get());
    }

    public List<WorkspaceDto> getWorkspaces() {
        List<Workspace> workspaces = workspaceRepository.findAll();

        return workspaces.stream().map(x -> CreateWorkspaceDto(x)).collect(Collectors.toList());
    }

    public WorkspaceDto getWorkspace(String workspaceUid) throws NotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();

        return CreateWorkspaceDto(dbWorkplace);
    }

    public void addRoomInWorkspace(String workspaceUid, RoomRequest request) throws NotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();
        dbWorkplace.addRoom(request);

        workspaceRepository.save(dbWorkplace);
    }

    public RoomDto updateRoomInWorkspace(String workspaceUid, String roomUid, RoomRequest request) throws NotFoundException, RoomNotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();
        dbWorkplace.updateRoom(roomUid, request);

        workspaceRepository.save(dbWorkplace);

        return CreateRoomDto(dbWorkplace.getRoom(roomUid));
    }

    public void deleteRoomInWorkspace(String workspaceUid, String roomUid) throws NotFoundException, RoomNotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();
        dbWorkplace.deleteRoom(roomUid);

        workspaceRepository.save(dbWorkplace);
    }

    public List<RoomDto> getRoomsInWorkspace(String workspaceUid) throws NotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();

        return dbWorkplace.getRooms().stream().map(x -> CreateRoomDto(x)).collect(Collectors.toList());
    }

    public RoomDto getRoomInWorkspace(String workspaceUid, String  roomUid) throws NotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();

        Room dbRoom = dbWorkplace.getRoom(roomUid);

        return CreateRoomDto(dbRoom);
    }

    public void addDeviceInRoom(String workspaceUid, String roomUid, DeviceRequest request) throws NotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();
        DeviceType deviceType = getDeviceType(request.getDeviceType());

        dbWorkplace.addDeviceInRoom(roomUid, request, deviceType);
        workspaceRepository.save(dbWorkplace);
    }

    public DeviceDto updateDeviceInRoom(String workspaceUid, String roomUid, String deviceUid, DeviceRequest request) throws NotFoundException, DeviceNotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();
        DeviceType deviceType = getDeviceType(request.getDeviceType());

        dbWorkplace.updateDeviceInRoom(roomUid, deviceUid, request, deviceType);
        workspaceRepository.save(dbWorkplace);

        return CreateDeviceDto(dbWorkplace.getDeviceInRoom(roomUid, deviceUid));
    }

    public void deleteDeviceInRoom(String workspaceUid, String roomUid, String deviceUid) throws NotFoundException, DeviceNotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();
        dbWorkplace.deleteDeviceInRoom(roomUid, deviceUid);

        workspaceRepository.save(dbWorkplace);
    }

    public List<DeviceDto> getDevicesInRoom(String workspaceUid, String roomUid) throws NotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();
        List<Device> dbDevices = dbWorkplace.getDevicesInRoom(roomUid);

        return dbDevices.stream().map(x -> CreateDeviceDto(x)).collect(Collectors.toList());
    }

    public DeviceDto getDeviceInRoom(String workspaceUid, String roomUid, String deviceUid) throws NotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();
        Device dbDevice = dbWorkplace.getDeviceInRoom(roomUid, deviceUid);

        return CreateDeviceDto(dbDevice);
    }

    public void addDeviceAttribute(String workspaceUid, String roomUid, String deviceUid, DeviceAttributeRequest request) throws NotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();
        dbWorkplace.addDeviceAttribute(roomUid, deviceUid, request);

        workspaceRepository.save(dbWorkplace);
    }

    public DeviceAttributeDto updateDeviceAttribute(String workspaceUid, String roomUid, String deviceUid, String deviceAttributeUid, DeviceAttributeRequest request) throws NotFoundException, DeviceAttributeNotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();
        dbWorkplace.updateDeviceAttribute(roomUid, deviceUid, deviceAttributeUid, request);

        workspaceRepository.save(dbWorkplace);

        return CreateDeviceAttributeDto(dbWorkplace.getDeviceAttribute(roomUid, deviceUid, deviceAttributeUid));
    }

    public void deleteDeviceAttribute(String workspaceUid, String roomUid, String deviceUid, String deviceAttributeUid) throws NotFoundException, DeviceAttributeNotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();
        dbWorkplace.removeDeviceAttribute(roomUid, deviceUid, deviceAttributeUid);

        workspaceRepository.save(dbWorkplace);
    }

    public List<DeviceAttributeDto> getDeviceAttributes(String workspaceUid, String roomUid, String deviceUid) throws NotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();
        List<DeviceAttribute> dbDeviceAttributes = dbWorkplace.getDeviceAttributes(roomUid, deviceUid);

        return dbDeviceAttributes.stream().map(x -> CreateDeviceAttributeDto(x)).collect(Collectors.toList());

    }

    public DeviceAttributeDto getDeviceAttribute(String workspaceUid, String roomUid, String deviceUid, String deviceAttributeUid) throws NotFoundException {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findByUid(workspaceUid);

        if (!optionalWorkspace.isPresent()) {
            throw new NotFoundException(String.format("Workplace with uid %s does not exist", workspaceUid));
        }

        Workspace dbWorkplace = optionalWorkspace.get();
        DeviceAttribute dbDeviceAttribute = dbWorkplace.getDeviceAttribute(roomUid, deviceUid, deviceAttributeUid);

        return CreateDeviceAttributeDto(dbDeviceAttribute);
    }

    private static WorkspaceDto CreateWorkspaceDto(Workspace workspace) {

        if (workspace == null) {
            return null;
        }

        WorkspaceDto workspaceDto = new WorkspaceDto();
        workspaceDto.setUid(workspace.getUid());
        workspaceDto.setName(workspace.getName());
        workspaceDto.setDescription(workspace.getDescription());

        return workspaceDto;
    }

    private static RoomDto CreateRoomDto(Room room) {

        if (room == null) {
            return null;
        }

        RoomDto roomDto = new RoomDto();
        roomDto.setUid(room.getUid());
        roomDto.setName(room.getName());
        roomDto.setFloor(room.getFloor());

        return roomDto;
    }

    private static DeviceDto CreateDeviceDto(Device device) {

        if (device == null) {
            return null;
        }

        DeviceDto deviceDto = new DeviceDto();
        deviceDto.setUid(device.getUid());
        deviceDto.setName(device.getName());
        deviceDto.setLocation(device.getLocation());
        deviceDto.setReferenceId(device.getReferenceId());
        deviceDto.setDeviceTypeId(device.getDeviceType().getId());

        return deviceDto;
    }

    private static DeviceAttributeDto CreateDeviceAttributeDto(DeviceAttribute deviceAttribute) {

        if (deviceAttribute == null) {
            return null;
        }

        DeviceAttributeDto deviceAttributeDto = new DeviceAttributeDto();
        deviceAttributeDto.setUid(deviceAttribute.getUid());
        deviceAttributeDto.setKey(deviceAttribute.getKey());
        deviceAttributeDto.setValue(deviceAttribute.getValue());

        return deviceAttributeDto;
    }

    private DeviceType getDeviceType(Long deviceTypeId) throws NotFoundException {
        Optional<DeviceType> optionalDeviceType = deviceTypeRepository.findById(deviceTypeId);

        if (!optionalDeviceType.isPresent()) {
            throw new NotFoundException(String.format("Device type with id %s does not exist", deviceTypeId));
        }

        return optionalDeviceType.get();
    }
}