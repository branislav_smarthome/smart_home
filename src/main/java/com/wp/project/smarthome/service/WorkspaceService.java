package com.wp.project.smarthome.service;

import com.wp.project.smarthome.contracts.*;

import com.wp.project.smarthome.repository.exceptions.DeviceAttributeNotFoundException;
import com.wp.project.smarthome.repository.exceptions.DeviceNotFoundException;
import com.wp.project.smarthome.repository.exceptions.RoomNotFoundException;
import javassist.NotFoundException;

import java.util.List;

public interface WorkspaceService {

    void createWorkspace(WorkspaceRequest request) throws NotFoundException;

    WorkspaceDto updateWorkspace(String workspaceUid, WorkspaceRequest request) throws NotFoundException;

    void deleteWorkspace(String workspaceUid) throws NotFoundException;

    List<WorkspaceDto> getWorkspaces();

    WorkspaceDto getWorkspace(String workspaceUid) throws NotFoundException;

    void addRoomInWorkspace(String workspaceUid, RoomRequest request) throws NotFoundException;

    RoomDto updateRoomInWorkspace(String workspaceUid, String roomUid, RoomRequest request) throws NotFoundException, RoomNotFoundException;

    void deleteRoomInWorkspace(String workspaceUid, String roomUid) throws NotFoundException, RoomNotFoundException;

    List<RoomDto> getRoomsInWorkspace(String workspaceUid) throws NotFoundException;

    RoomDto getRoomInWorkspace(String workspaceUid, String  roomUid) throws NotFoundException;

    void addDeviceInRoom(String workspaceUid, String roomUid, DeviceRequest request) throws NotFoundException;

    DeviceDto updateDeviceInRoom(String workspaceUid, String roomUid, String deviceUid, DeviceRequest request) throws NotFoundException, DeviceNotFoundException;

    void deleteDeviceInRoom(String workspaceUid, String roomUid, String deviceUid) throws NotFoundException, DeviceNotFoundException;

    List<DeviceDto> getDevicesInRoom(String workspaceUid, String roomUid) throws NotFoundException;

    DeviceDto getDeviceInRoom(String workspaceUid, String roomUid, String deviceUid) throws NotFoundException;

    void addDeviceAttribute(String workspaceUid, String roomUid, String deviceUid, DeviceAttributeRequest request) throws NotFoundException;

    DeviceAttributeDto updateDeviceAttribute(String workspaceUid, String roomUid, String deviceUid, String deviceAttributeUid, DeviceAttributeRequest request) throws NotFoundException, DeviceAttributeNotFoundException;

    void deleteDeviceAttribute(String workspaceUid, String roomUid, String deviceUid, String deviceAttributeUid) throws NotFoundException, DeviceAttributeNotFoundException;

    List<DeviceAttributeDto> getDeviceAttributes(String workspaceUid, String roomUid, String deviceUid) throws NotFoundException;

    DeviceAttributeDto getDeviceAttribute(String workspaceUid, String roomUid, String deviceUid, String deviceAttributeUid) throws NotFoundException;
}
