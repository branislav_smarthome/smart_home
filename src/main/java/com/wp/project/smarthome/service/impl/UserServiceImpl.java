package com.wp.project.smarthome.service.impl;

import com.wp.project.smarthome.models.User;
import com.wp.project.smarthome.repository.UserRepository;
import com.wp.project.smarthome.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void postConstruct() {

        User user = new User();
        user.setFirstName("Branislav");
        user.setLastName("Petrushevski");
        userRepository.save(user);
    }
}