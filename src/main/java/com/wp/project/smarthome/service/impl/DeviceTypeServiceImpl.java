package com.wp.project.smarthome.service.impl;

import com.wp.project.smarthome.contracts.DeviceTypeDto;
import com.wp.project.smarthome.models.DeviceType;
import com.wp.project.smarthome.repository.DeviceTypeRepository;
import com.wp.project.smarthome.service.DeviceTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DeviceTypeServiceImpl implements DeviceTypeService {

    private DeviceTypeRepository deviceTypeRepository;

    public DeviceTypeServiceImpl(DeviceTypeRepository deviceTypeRepository) {
        this.deviceTypeRepository = deviceTypeRepository;
    }

    @PostConstruct
    public void postConstruct() {

        DeviceType deviceType = new DeviceType();
        deviceType.setName("Osvetluvanje");
        deviceTypeRepository.save(deviceType);
    }

    public List<DeviceTypeDto> getDeviceTypes() {
        List<DeviceType> dbDeviceTypes = deviceTypeRepository.findAll();

        return dbDeviceTypes.stream().map(x -> CreateDeviceTypeDto(x)).collect(Collectors.toList());
    }

    public static DeviceTypeDto CreateDeviceTypeDto(DeviceType deviceType) {

        if (deviceType == null) {
            return null;
        }

        DeviceTypeDto deviceTypeDto = new DeviceTypeDto();
        deviceTypeDto.setId(deviceType.getId());
        deviceTypeDto.setName(deviceType.getName());

        return deviceTypeDto;
    }
}
