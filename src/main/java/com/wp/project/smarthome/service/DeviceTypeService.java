package com.wp.project.smarthome.service;

import com.wp.project.smarthome.contracts.DeviceTypeDto;

import java.util.List;

public interface DeviceTypeService {

    List<DeviceTypeDto> getDeviceTypes();
}
